<?php
function flatten(array $array) {
    $return = [];
    //array_walk_recursive($array, function($a) use (&$return) { $return[] = $a; });
    array_walk_recursive($array, function ($val, $key) use (&$return) {
        $return[$key] = $val;
    });
    return $return;
}

function flattenInner(array $array) {
    $return = [];
    foreach ($array as $subArray) {
        foreach ($subArray as $key => $value) {
            if (is_array($value)) {
                $return[$key] = $value;
            } else {
                $return[$key] = $value;
            }
        }
    }
    return $return;
}

function dibiSquish($result) {
    return flattenInner(json_decode(json_encode($result), true));
}

function dibiToArray($query) {
    return json_decode(json_encode($query), true);
}

/**
 * @param $captchaResponse
 * @return bool
 */
function verifyRecaptcha($captchaResponse): bool{
    if (empty($captchaResponse)) {
        return false;
    }
    $secretKey = "6LcjmocnAAAAAJXl-jLU5N6-LH7L8IyUn7Hj9Ge0";
    $ip = $_SERVER['REMOTE_ADDR'];
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$captchaResponse&remoteip=$ip";
    $response = file_get_contents($url);
    $isSuccess = json_decode($response, true)['success'];

    return $isSuccess;
}