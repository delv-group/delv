<?php

define('ROOTPATH', $_SERVER["DOCUMENT_ROOT"]);
define("HTTP_PATH_ROOT", $_SERVER["HTTP_HOST"] ?? ($_SERVER["SERVER_NAME"] ?? '_UNKNOWN_'));

date_default_timezone_set('Europe/Moscow');

// 1. Session

// server should keep session data for AT LEAST 6 hours
$lifetime = 21600;

if (session_status() == PHP_SESSION_NONE) {
    session_start([
        'cookie_lifetime' => $lifetime,
    ]);
}

// 2. Includes
require_once ROOTPATH . "/vendor/autoload.php";
require "functions.php";
require_once "user.class.php";

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;


// 3. DB
try {
    $db_yaml = Yaml::parseFile("db.yaml");
    $database = new Dibi\Connection($db_yaml["database"]);
    $user = new Delv\User($database);
} catch (ParseException $exception) {
    printf('Unable to parse the YAML string: %s', $exception->getMessage());
}

// 4. Whoops
$whoops = new \Whoops\Run;
$whoops->prependHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

// 5. Templates

$themes_path = "/themes";

$theme = [
    "theme_name" => "delv_v1",
    "theme_path" => "%s/%s",
    "theme_assets_path" => "%s/assets",
    "theme_templates_path" => "%s/tpl"
];

$theme["theme_path"] = sprintf($theme["theme_path"],$themes_path,$theme["theme_name"]);
$theme["theme_assets_path"] = sprintf($theme["theme_assets_path"],$theme["theme_path"]);
$theme["theme_templates_path"] = sprintf($theme["theme_templates_path"],$theme["theme_path"]);

//die(var_dump($theme));
$template_user_session = $user->getUserSession();
$template_user_data = $user->getUserData();

$template_data =  [
    "theme" => $theme,
    "user" => [
        "isLoggedIn" => $user->isLoggedIn(),
        "session" => $template_user_session,
        "data" => $template_user_data
    ],
    "communities_data" => dibiToArray($database->query("select * from communities")->fetchAll())
];

use Twig\Extra\Intl\IntlExtension;
use Twig\Extra\Markdown\DefaultMarkdown;
use Twig\Extra\Markdown\MarkdownExtension;
use Twig\Extra\Markdown\MarkdownRuntime;
use buzzingpixel\twigswitch\SwitchTwigExtension;

$loader = new \Twig\Loader\FilesystemLoader($_SERVER["DOCUMENT_ROOT"] . $theme['theme_templates_path']);
$twig = new \Twig\Environment($loader, [
    'cache' => 'tpl_c',
    'debug' => true
]);

$twig->addGlobal('tpl', $template_data);
$twig->addGlobal('session', $_SESSION);
$twig->addExtension(new \Twig\Extension\DebugExtension());
$twig->addExtension(new IntlExtension());

$twig->addRuntimeLoader(new class implements \Twig\RuntimeLoader\RuntimeLoaderInterface {
    public function load($class)
    {
        if (MarkdownRuntime::class === $class) {
            return new MarkdownRuntime(new DefaultMarkdown());
        }
    }
});
$twig->addExtension(new MarkdownExtension());
$twig->addExtension(new Kint\Twig\TwigExtension());

$twig__usort = new \Twig\TwigFilter('usort', function ($item) {
    usort($item, function ($item1, $item2) {
        if ($item1['orderNo'] == $item2['orderNo']) return 0;
        return $item1['orderNo'] < $item2['orderNo'] ? -1 : 1;
    });

    return $item;
});
$twig->addFilter($twig__usort);

$twig__timeago = new \Twig\TwigFilter('timeago', function ($datetime) {

    $time = time() - strtotime($datetime);

    $units = array(
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($units as $unit => $val) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        if (($val == 'second')) {
            return 'a few seconds ago';
        } else if (($val == 'hour')) {
            return (($numberOfUnits > 1) ? $numberOfUnits : 'an')
                . ' ' . $val . (($numberOfUnits > 1) ? 's' : '') . ' ago';
        } else {
            return (($numberOfUnits > 1) ? $numberOfUnits : 'a')
                . ' ' . $val . (($numberOfUnits > 1) ? 's' : '') . ' ago';
        }
    }

});
$twig->addFilter($twig__timeago);

$twig__initials = new \Twig\TwigFunction('initials', function ($string) {
    return initials($string);
});
$twig->addFunction($twig__initials);

$twig->addExtension(new SwitchTwigExtension());

$twig__pluralize = new \Twig\TwigFunction('pluralize', function (int $count, string $singular, string $plural, string $zero = null):string {
    if ($count > 1){
        return str_replace('{}', $count, $plural);
    } else if ($count <= 0 && null !== $zero){
        return $zero; // No string replacement required for zero
    }
    return str_replace('{}', $count, $singular);
});
$twig->addFunction($twig__pluralize);
$twig__json_decode = new \Twig\TwigFunction('json_decode', function (string $json, bool $assoc = true, int $depth = 512, int $options = 0) {
    return json_decode($json, $assoc, $depth, $options);
});
$twig->addFunction($twig__json_decode);
$twig__is_json = new \Twig\TwigFunction('is_json', function ($string) {
    json_decode($string);
    return json_last_error() === JSON_ERROR_NONE;
});
$twig->addFunction($twig__is_json);