<?php

use Ramsey\Uuid\Uuid;
use Delv\User;

/*
 * Switch to fetch/fetchAll
 * $database->fetch("...")
 */

$klein->respond('GET', '/hello', function ($request,$response) use ($database,$twig) {
    //echo $request->username;
    echo <<<html
        <p>Hello from delv!</p>
        <p>Here are the available communities:</p> 
    html;
    print_r(dibiToArray($database->fetchAll("select name from communities")));
    echo $twig->render("hello.twig",[]);
});

$klein->respond('GET', '/register', function ($request,$response) use ($database,$twig) {
    //echo $request->username;
    echo $twig->render("register.twig",["page__title"=>"Delv - Register","page__body__sidebars"=>"none"]);
});

$klein->respond('POST', '/register', function ($request,$response) use ($database,$twig) {
    if ($_SERVER['REQUEST_METHOD'] === 'POST'){
        //die(var_dump($_POST));
        if(verifyRecaptcha($_POST['g-recaptcha-response'])){
            if(!empty($_POST)){
                /*
                $database->query('INSERT INTO users', [
                    'username' => $_POST['login'],
                    'display_name' => $_POST['display_name'],
                    'password_hash' => hash('sha3-256',$_POST['password']),
                    'email' => $_POST['email'],
                    'extra' => json_encode($extra_def),
                    'rpg' => json_encode($rpg_def),
                    'uuid' => $uuid->toString()
                ]);

                $database->query('INSERT INTO users_roles', [
                    'user_uuid' => $uuid->toString(),
                    'role_id' => 4
                ]);
                */
                $user = new Delv\User();
                $user->userRegister($_POST);
                $response->redirect("/");
            } else {
                die("Empty registration form");
            }
        } else {
            $response->redirect("/register");
        }
    }
});
$klein->respond('GET', '/login', function ($request,$response) use ($database,$twig) {
    //echo $request->username;
    echo $twig->render("login.twig",["page__title"=>"Delv - Login","page__body__sidebars"=>"none"]);
});
/* WARNING, COPY-PASTE, EDIT FIRST!!! */
$klein->respond('POST', '/login', function ($request,$response) use ($database,$twig) {
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $user = new Delv\User();
        $user->userLogin($_POST);
        $response->redirect("/");
    } else {
        die('Empty form');
    }
});
/* !!! */

$klein->respond('GET', '/logout', function ($request,$response) use ($database,$twig,$klein) {
    if (!empty($_SESSION)) {
        //session_unset();
        session_destroy();
        $response->redirect("/");
    } else {
        $response->redirect("/login");
    }
    //exit();
});


$klein->respond('GET', '(/?|/home)', function ($request, $response) use ($database, $twig) {
    $posts_all = dibiToArray($database->query("select * from posts order by date_posted desc")->fetchAll());
    foreach ($posts_all as $k=>$v){
        $posts_all[$k]['_comments']['_number'] = dibiToArray($database->query("select * from comments where post_id = ?", $posts_all[$k]["id"])->count());
        $posts_all[$k]['_author'] = dibiSquish($database->query("select * from users where id = ?", $posts_all[$k]["author_id"])->fetchAll());
        $posts_all[$k]['_community'] = dibiSquish($database->query("select * from communities where id = ?", $posts_all[$k]["community_id"])->fetchAll());
        if(!empty($posts_all[$k]['source'])){
            $posts_all[$k]['source'] = [
                "url" => $posts_all[$k]['source'],
                "domain" => parse_url($posts_all[$k]['source'])["scheme"]."://".parse_url($posts_all[$k]['source'])["host"]
            ];
        } else {
            $posts_all[$k]['source'] = null;
        }
        $parsedown = new Parsedown();
        $parsedown->setSafeMode(true);
        $posts_all[$k]['content_teaser'] = $parsedown->text($posts_all[$k]['content_teaser']);
        $posts_all[$k]['content_full'] = $parsedown->text($posts_all[$k]['content_full']);

        unset($posts_all[$k]["author_id"]);
        unset($posts_all[$k]["community_id"]);
    }
    $date = strtotime("August 7, 2023 19:00");
    $remaining = $date - time();
    $days_remaining = floor($remaining / 86400);
    $hrs_remaining  = floor(($remaining % 86400) / 3600);
    $mins_remaining = floor(($remaining % 3600) / 60);
    $secs_remaining = ($remaining % 60);

    if($date > time()){
        //$count = sprintf("<h1>Currently remaining</h1> <p>%s days, %s hours, %s minutes & %s seconds</p><br/>",$days_remaining,$hrs_remaining,$mins_remaining,$secs_remaining);
        $timer = <<<html
            <!doctype html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport"
                      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>Delv - Oooh, it's almost time!</title>
                <!-- Meta tags -->
                <meta name="description" content="">
                <!-- /Meta tags -->
                <!-- External styles and scripts -->
                <link rel="stylesheet" href="https://delv.aolko.me/themes/delv_v1/assets/styles/static.css">
                <script src="https://delv.aolko.me/themes/delv_v1/assets/fa/fontawesome.min.js" data-auto-replace-svg="nest"></script>
                <script src="https://delv.aolko.me/themes/delv_v1/assets/fa/solid.min.js"></script>
                <script src="https://delv.aolko.me/themes/delv_v1/assets/fa/regular.min.js"></script>
                <script src="https://delv.aolko.me/themes/delv_v1/assets/fa/brands.min.js"></script>
                <script src="https://code.iconify.design/iconify-icon/1.0.8/iconify-icon.min.js"></script>
                <!-- /External styles and scripts -->
                <!-- Internal styles and scripts -->
                <!-- /Internal styles and scripts -->
            </head>
            <body>
            
                <div class="page-container">
                    <header class="page__header">
                        <div class="logo">
                            <svg height="24" viewBox="0 0 230 160" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <g id="Delv/Logo">
                                    <path id="logo" fill-rule="evenodd" clip-rule="evenodd" d="M0 120H60V0H40V40H0V120ZM20 60H40V100H20V60ZM90 140V120H130V40H70V160H130V140H90ZM90 60H110V100H90V60ZM160 0H140V120H160V0ZM190 40H170V120H210V100H230V40H210V100H190V40Z" fill="white"/>
                                </g>
                            </svg>
                        </div>
                        <div class="heading">Whoa!</div>
                    </header>
                    <div class="page__body">
                        <h1>Currently remaining</h1>
                        <p id="count" style="font-size: 1.2rem;"></p>
                        <br>
                        <video src="dw.mp4" style="height:400px;width:100%;object-fit:cover;" controls></video>
                    </div>
                </div>
                <script>
                    // Set the date we're counting down to
                    var countDownDate = new Date("Aug 7, 2023 19:00:00").getTime();
                    
                    // Update the count down every 1 second
                    var x = setInterval(function() {
                    
                      // Get today's date and time
                      var now = new Date().getTime();
                    
                      // Find the distance between now and the count down date
                      var distance = countDownDate - now;
                    
                      // Time calculations for days, hours, minutes and seconds
                      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                    
                      // Display the result in the element with id="demo"
                      document.getElementById("count").innerHTML = days + " days " + hours + " hours "
                      + minutes + " minutes & " + seconds + " seconds ";
                    
                      // If the count down is finished, write some text
                      if (distance < 0) {
                        clearInterval(x);
                        //document.getElementById("count").innerHTML = "EXPIRED";
                        window.location.replace("/");
                      }
                    }, 1000);
                </script>
            </body>
            </html>
        html;

        echo $timer;
        //http_response_code(425);
        //die(file_get_contents("delv.html"));
    } else {
        echo $twig->render("home.twig",["page__title"=>"Delv - Home","page__body__sidebars"=>"left","posts" => $posts_all]);
    }
});

$klein->respond('GET', '/submit', function ($request, $response) use ($database, $twig) {
    echo $twig->render("submit.twig",["page__title"=>"Delv - Submit post","page__body__sidebars"=>"none"]);
});
$klein->respond('POST', '/submit', function ($request, $response) use ($database, $twig) {
    $user = new Delv\User();
    $user_session = $user->getUserSession();
    $community = dibiSquish($database->fetchAll("select * from communities where id = ?",$_POST['community']));
    $database->query("insert into posts",[
        "name"=>$_POST['post-name'],
        "content_teaser"=>$_POST['post-content-teaser'],
        "content_full"=>$_POST['post-content-full'],
        "author_id"=>$user_session['id'],
        "community_id"=>$community['id']
    ]);
    $response->redirect('/');
});

$klein->with('/c/[:community_slug]', function () use ($klein,$database,$twig) {

    $klein->respond('GET', '/?', function ($request, $response) use ($database, $twig) {
        $community = dibiSquish($database->query("select * from communities where slug = ?",$request->community_slug)->fetchAll());
        $community_users = dibiToArray($database->query("select * from users_communities where community_id = ?",$community["id"])->count());
        $community["users"] = $community_users;
        $posts_all = dibiToArray($database->query("select * from posts where community_id = ? order by date_posted desc",$community["id"])->fetchAll());
        foreach ($posts_all as $k=>$v){
            $posts_all[$k]['_comments']['_number'] = dibiToArray($database->query("select * from comments where post_id = ?", $posts_all[$k]["id"])->count());
            $posts_all[$k]['_author'] = dibiSquish($database->query("select * from users where id = ?", $posts_all[$k]["author_id"])->fetchAll());
            $posts_all[$k]['_community'] = dibiSquish($database->query("select * from communities where id = ?", $posts_all[$k]["community_id"])->fetchAll());
            if(!empty($posts_all[$k]['source'])){
                $posts_all[$k]['source'] = [
                    "url" => $posts_all[$k]['source'],
                    "domain" => parse_url($posts_all[$k]['source'])["scheme"]."://".parse_url($posts_all[$k]['source'])["host"]
                ];
            } else {
                $posts_all[$k]['source'] = null;
            }
            $parsedown = new Parsedown();
            $parsedown->setSafeMode(true);
            $posts_all[$k]['content_teaser'] = $parsedown->text($posts_all[$k]['content_teaser']);
            $posts_all[$k]['content_full'] = $parsedown->text($posts_all[$k]['content_full']);
            unset($posts_all[$k]["author_id"]);
            unset($posts_all[$k]["community_id"]);
        }
        echo $twig->render("community.twig",["page__title"=>"Delv - {$community['name']}","page__body__sidebars"=>"both","community"=>$community,"posts" => $posts_all]);
    });
    $klein->respond('GET', '/posts', function ($request, $response) use ($database, $twig) {

    });
    $klein->respond('GET', '/posts/[:post_id]', function ($request, $response) use ($database, $twig) {
        $community = dibiSquish($database->query("select * from communities where slug = ?",$request->community_slug)->fetchAll());
        $community_users = dibiToArray($database->query("select * from users_communities where community_id = ?",$community["id"])->count());
        $community["users"] = $community_users;
        //die(d($request->community_slug));
        $post = dibiSquish($database->query("select * from posts where id = ?",$request->post_id)->fetchAll());
        $community_name = $community["name"];
        $post_name = $post["name"];
        //die(d($post));
        $post['_author'] = dibiSquish($database->query("select * from users where id = ?", $post["author_id"])->fetchAll());
        $post['_community'] = dibiSquish($database->query("select * from communities where id = ?", $post["community_id"])->fetchAll());
        $post['_comments'] = dibiToArray($database->query("select * from comments where post_id = ?", $post["id"])->fetchAll());
        /*foreach ($post['_comments'] as $k=>$v){
            $post['_comments'][$k]["_author"] = dibiSquish($database->query("select * from users where id = ?", $post['_comments'][$k]["author_id"])->fetchAll());
            unset($post['_comments'][$k]["post_id"]);
            unset($post['_comments'][$k]["author_id"]);
        }*/
        if(!empty($post['source'])){
            $post['source'] = [
                "url" => $post['source'],
                "domain" => parse_url($post['source'])["scheme"]."://".parse_url($post['source'])["host"]
            ];
        } else {
            $post['source'] = null;
        }
        $parsedown = new Parsedown();
        $parsedown->setSafeMode(true);
        $post['content_teaser'] = $parsedown->text($post['content_teaser']);
        $post['content_full'] = $parsedown->text($post['content_full']);
        unset($post["author_id"]);
        unset($post["community_id"]);
        //d($post);
        //echo dibi::$sql;

        echo $twig->render("post.twig",["page__title"=>"Delv - {$community_name}: {$post_name}","page__body__sidebars"=>"both","community" => $community,"post" => $post]);
    });
    $klein->respond('GET', '/posts/[:post_id]/score', function ($request, $response) use ($database) {
        $post = dibiSquish($database->query("select * from posts where id = ?",$request->post_id)->fetchAll());
        echo $post['score'];
    });
    $klein->respond('POST', '/posts/[:post_id]/[:interaction]', function ($request, $response) use ($database) {
        $post = dibiSquish($database->query("select * from posts where id = ?",$request->post_id)->fetchAll());
        $user = dibiSquish($database->query("select * from users where id = ?", $post["author_id"])->fetchAll());
        $post_id = $request->post_id;
        function getContentInteraction($post_id,$userIP,$type){
            global $database;
            return dibiSquish($database->query("select * from content_interactions where content_id=? and user_ip = ? and interaction_type = ?", $post_id, $userIP, $type)->fetchAll());
        }
        function hasUserVoted($post_id,$userIP,$type){
            $interactions = getContentInteraction($post_id, $userIP, $type);
            return !empty($interactions);
        }
        if(!empty($request->interaction)){
            $interactionType = $request->interaction;
            $userIP = $_SERVER['REMOTE_ADDR'];
            //die(d(hasUserVoted($post_id,$userIP,$interactionType)));
            if (!hasUserVoted($post_id, $userIP, $interactionType)) {
                switch ($request->interaction){
                    case "upvote":
                        $database->query("update posts set",[
                            "score" => $post['score'] + 1
                        ],"where id = ?",$post_id);
                        $database->query("insert into content_interactions",[
                            "content_type"=>"post",
                            "content_id"=>$post_id,
                            "interaction_type"=>"upvote",
                            "user_ip"=>$_SERVER['REMOTE_ADDR'],
                            "user_id"=>$user['id']
                        ]);
                        break;
                    case "downvote":
                        $database->query("update posts set",[
                            "score" => $post['score'] - 1
                        ],"where id = ?",$post_id);
                        $database->query("insert into content_interactions",[
                            "content_type"=>"post",
                            "content_id"=>$post_id,
                            "interaction_type"=>"downvote",
                            "user_ip"=>$_SERVER['REMOTE_ADDR'],
                            "user_id"=>$user['id']
                        ]);
                        break;
                }
            } else {
                echo "vote_exists";
            }

            //echo "vote_new";
            /*
            if ($request->interaction == "score"){
                echo $post['score'];
            } else {
                if (in_array($request->post_id, $contentInteraction)) {
                    echo "vote_exists";
                } else {
                    //echo "not_active";
                    switch ($request->interaction){
                        case "upvote":
                            $database->query("update posts set",[
                                "score" => $post['score'] + 1
                            ],"where id = ?",$request->post_id);
                            $database->query("insert into content_interactions",[
                                "content_type"=>"post",
                                "content_id"=>$request->post_id,
                                "interaction_type"=>"upvote",
                                "user_ip"=>$_SERVER['REMOTE_ADDR'],
                                "user_id"=>$user['id']
                            ]);
                            break;
                        case "downvote":
                            $database->query("update posts set",[
                                "score" => $post['score'] - 1
                            ],"where id = ?",$request->post_id);
                            $database->query("insert into content_interactions",[
                                "content_type"=>"post",
                                "content_id"=>$request->post_id,
                                "interaction_type"=>"downvote",
                                "user_ip"=>$_SERVER['REMOTE_ADDR'],
                                "user_id"=>$user['id']
                            ]);
                            break;
                    }
                    echo "vote_new";
                }
            }
            */
        }
    });
    $klein->respond('POST', '/posts/[:post_id]/comment', function ($request, $response) use ($database,$twig) {
        $user = new Delv\User();
        $user_session = $user->getUserSession();
        if (!empty($_POST['comment_content'])){
            $content = $_POST['comment_content'];
        } else {
            $content = "Whoops misclicked";
        }
        if(!empty($user_session)){
            $database->query("insert into comments",[
                "name"=>$_POST['comment_name'],
                "content"=>$content,
                "author_id"=>$user_session['id'],
                //"author_id"=>1,
                "post_id"=>$request->post_id
            ]);
        } else {
            $database->query("insert into comments",[
                "name"=>$_POST['comment_name'],
                "content"=>$content,
                "anon_author_name"=>"anonymous",
                "anon_author_avatar"=>null,
                "post_id"=>$request->post_id,
                "author_id" => -1,
                "is_anonymous" => 1
            ]);
        }


        /*$comments = dibiToArray($database->query("select * from comments where post_id = ?", $request->post_id)->fetchAll());
        foreach ($comments as $k=>$v){
            $comments[$k]["_author"] = dibiSquish($database->query("select * from users where id = ?", $comments[$k]["author_id"])->fetchAll());
            unset($comments[$k]["post_id"]);
            unset($comments[$k]["author_id"]);
        }
        return $twig->render("_comments.twig",["comments"=>$comments]);*/
    });
    $klein->respond('GET', '/posts/[:post_id]/comments', function ($request, $response) use ($database,$twig) {
        $community = dibiSquish($database->query("select * from communities where slug = ?",$request->community_slug)->fetchAll());
        $post = dibiSquish($database->query("select * from posts where id = ?",$request->post_id)->fetchAll());
        $post['_author'] = dibiSquish($database->query("select * from users where id = ?", $post["author_id"])->fetchAll());
        $post['_community'] = dibiSquish($database->query("select * from communities where id = ?", $post["community_id"])->fetchAll());
        $post['_comments'] = dibiToArray($database->query("select * from comments where post_id = ?", $post["id"])->fetchAll());
        if(!empty($post['source'])){
            $post['source'] = [
                "url" => $post['source'],
                "domain" => parse_url($post['source'])["scheme"]."://".parse_url($post['source'])["host"]
            ];
        } else {
            $post['source'] = null;
        }
        $parsedown = new Parsedown();
        $parsedown->setSafeMode(true);
        $post['content_teaser'] = $parsedown->text($post['content_teaser']);
        $post['content_full'] = $parsedown->text($post['content_full']);
        unset($post["author_id"]);
        unset($post["community_id"]);

        $comments = dibiToArray($database->query("select * from comments where post_id = ? order by date_posted desc", $request->post_id)->fetchAll());
        foreach ($comments as $k=>$v){
            $parsedown = new Parsedown();
            $parsedown->setSafeMode(true);
            $comments[$k]['content'] = $parsedown->text($comments[$k]['content']);
            $comments[$k]["_author"] = dibiSquish($database->query("select * from users where id = ?", $comments[$k]["author_id"])->fetchAll());
            unset($comments[$k]["post_id"]);
            unset($comments[$k]["author_id"]);
        }
        //$comments['_comments_number'] = dibiToArray($database->query("select * from comments where post_id = ?", $request->post_id)->count());
        //d($comments);
        //header("Content-Type: application/json");
        //echo json_encode($comments);
        //exit();
        echo $twig->render("_comments.twig",["comments"=>$comments,"community"=>$community,"post"=>$post]);
    });
    $klein->respond('POST', '/posts/[:post_id]/comments/[:comment_id]/[:interaction]', function ($request, $response) use ($database) {
        $comments = dibiToArray($database->query("select * from comments where post_id = ? order by date_posted desc", $request->post_id)->fetchAll());
        $comment = dibiSquish($database->query("select * from comments where post_id = ? and id=? order by date_posted desc", $request->post_id, $request->comment_id)->fetchAll());
        $user = dibiSquish($database->query("select * from users where id = ?", $comment["author_id"])->fetchAll());
        $comment_id = $request->comment_id;
        //die(d($comment["author_id"], $user));
        function getContentInteraction($content_type,$comment_id,$userIP,$type){
            global $database;
            return dibiSquish($database->query("select * from content_interactions where content_Type=? and content_id=? and user_ip = ? and interaction_type = ?", $content_type, $comment_id, $userIP, $type)->fetchAll());
        }
        function hasUserVoted($content_type,$comment_id,$userIP,$type){
            $interactions = getContentInteraction($content_type, $comment_id, $userIP, $type);
            return !empty($interactions);
        }
        if(!empty($request->interaction)){
            $interactionType = $request->interaction;
            $userIP = $_SERVER['REMOTE_ADDR'];
            //die(d(hasUserVoted($post_id,$userIP,$interactionType)));
            if (!hasUserVoted("comment", $comment_id, $userIP, $interactionType)) {
                switch ($request->interaction){
                    case "upvote":
                        $database->query("update comments set",[
                            "score" => $comment['score'] + 1
                        ],"where id = ?",$comment_id);
                        $database->query("insert into content_interactions",[
                            "content_type"=>"comment",
                            "content_id"=>$comment_id,
                            "interaction_type"=>"upvote",
                            "user_ip"=>$_SERVER['REMOTE_ADDR'],
                            "user_id"=>$user['id']
                        ]);
                        break;
                    case "downvote":
                        $database->query("update comments set",[
                            "score" => $comment['score'] - 1
                        ],"where id = ?",$comment_id);
                        $database->query("insert into content_interactions",[
                            "content_type"=>"comment",
                            "content_id"=>$comment_id,
                            "interaction_type"=>"downvote",
                            "user_ip"=>$_SERVER['REMOTE_ADDR'],
                            "user_id"=>$user['id']
                        ]);
                        break;
                }
            } else {
                echo "vote_exists";
            }

            //echo "vote_new";
            /*
            if ($request->interaction == "score"){
                echo $post['score'];
            } else {
                if (in_array($request->post_id, $contentInteraction)) {
                    echo "vote_exists";
                } else {
                    //echo "not_active";
                    switch ($request->interaction){
                        case "upvote":
                            $database->query("update posts set",[
                                "score" => $post['score'] + 1
                            ],"where id = ?",$request->post_id);
                            $database->query("insert into content_interactions",[
                                "content_type"=>"post",
                                "content_id"=>$request->post_id,
                                "interaction_type"=>"upvote",
                                "user_ip"=>$_SERVER['REMOTE_ADDR'],
                                "user_id"=>$user['id']
                            ]);
                            break;
                        case "downvote":
                            $database->query("update posts set",[
                                "score" => $post['score'] - 1
                            ],"where id = ?",$request->post_id);
                            $database->query("insert into content_interactions",[
                                "content_type"=>"post",
                                "content_id"=>$request->post_id,
                                "interaction_type"=>"downvote",
                                "user_ip"=>$_SERVER['REMOTE_ADDR'],
                                "user_id"=>$user['id']
                            ]);
                            break;
                    }
                    echo "vote_new";
                }
            }
            */
        }
    });
    $klein->respond('GET', '/posts/[:post_id]/comments/[:comment_id]/score', function ($request, $response) use ($database) {
        $comments = dibiToArray($database->query("select * from comments where post_id = ? order by date_posted desc", $request->post_id)->fetchAll());
        $comment = dibiSquish($database->query("select * from comments where post_id = ? and id=? order by date_posted desc", $request->post_id, $request->comment_id)->fetchAll());
        echo $comment['score'];
    });
});

$klein->respond('GET', '/'.urlencode("🔐").'/[:pw]', function ($request, $response) {
    return var_dump(password_hash($request->pw,PASSWORD_BCRYPT));
});