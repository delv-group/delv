<?php
require_once "init.php";

use Klein\Klein;

$klein = new Klein();

require_once "routes.php";

$klein->dispatch();
