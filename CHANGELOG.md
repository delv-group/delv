## Alpha1
### 08.08.2023
- [+] Add twemojis
- [~] Unified post templates
- [+] Digs are now working so far
- [+] Bury works too
- [~] Score updates on dig/bury but leaky (fixed)
- [~] Some elements are now inactive for anonymous users
- [~] Moved "My" to profile page
- [+] Added "Labs"
- [~] Added external links to the left sidebar

### 09.08.2023
- [+] Added a message